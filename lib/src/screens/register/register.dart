import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  TextEditingController _usernameController;

  @override
  void initState() {
    // TODO: implement initState
    _usernameController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            // background section
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/clarisse.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(

                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                  ]
                )
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  // top bar section
                  Container(
                    padding: EdgeInsets.only(top: 48.14, left: 25.0),
                    alignment: Alignment.topCenter,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(width: 120,),
                        Text(
                          'Register',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontSize: 17,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),

                  // middle bar section
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Create an account',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontSize: 28.0,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                        SizedBox(height: 15.0,),
                        Text(
                          'Signing up or login to see\nour top picks for you',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Roboto',
                            fontSize: 17.0,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w300
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),

                  // bottom bar section
                  Container(
                    margin: EdgeInsets.all(25.0),
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please fill the password field box';
                            } else {
                              return null;
                            }
                          },
                          controller: _usernameController,
                          decoration: InputDecoration(
                            filled: true,
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black
                              ),
                            ),
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue)
                            ),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.red)
                            ),
                            labelText: 'Enter Username',
                            labelStyle: TextStyle(color: Colors.black),
                            prefixIcon: Icon(Icons.person, color: Colors.black,),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 20.0),
                          height: 60,
                          decoration: BoxDecoration(
                            color: Color(0xFF0084F4),
                            borderRadius: BorderRadius.circular(5.0)
                          ),
                          width: MediaQuery.of(context).size.width,
                          child: FlatButton(
                            onPressed: () {
                              Navigator.pushNamed(context, '/emailRegister');
                            }, 
                            child: Text(
                              'Next', 
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Roboto',
                                fontSize: 16.0,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w300
                              )
                            )
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 16.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/login');
                            },
                            child: Text(
                              'I have account! Login',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Roboto',
                                fontSize: 15,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal
                              ),
                            ),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 16.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height: 8.0,
                                width: 8.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xFF0084F4)
                                ),
                              ),
                              SizedBox(width: 10.0,),
                              Container(
                                height: 8.0,
                                width: 8.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xFFE4E4E4)
                                ),
                              ),
                              SizedBox(width: 10.0,),
                              Container(
                                height: 8.0,
                                width: 8.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xFFE4E4E4)
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}

class EmailRegister extends StatefulWidget {
  String username;
  EmailRegister({this.username, Key key}) : super(key: key);

  @override
  _EmailRegisterState createState() => _EmailRegisterState();
}

class _EmailRegisterState extends State<EmailRegister> {

  TextEditingController _emailController;

  @override
  void initState() {
    // TODO: implement initState
    _emailController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // background section
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/stil.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(

                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                  ]
                )
              ),
            ),

          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                // top bar section
                Container(
                  padding: EdgeInsets.only(top: 48.14, left: 25.0),
                  alignment: Alignment.topCenter,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(width: 120,),
                      Text(
                        'Register',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),

                // middle bar section
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Create an account',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 28.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      SizedBox(height: 15.0,),
                      Text(
                        'Signing up or login to see\nour top picks for you',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 17.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w300
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),

                // bottom bar section
                Container(
                  margin: EdgeInsets.all(25.0),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please fill the email field box';
                          } else {
                            return null;
                          }
                        },
                        controller: _emailController,
                        decoration: InputDecoration(
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.black
                            ),
                          ),
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue)
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red)
                          ),
                          labelText: 'Enter Your Email Address',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(Icons.email, color: Colors.black,),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 20.0),
                        height: 60,
                        decoration: BoxDecoration(
                          color: Color(0xFF0084F4),
                          borderRadius: BorderRadius.circular(5.0)
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/passwordRegister');
                          }, 
                          child: Text(
                            'Next', 
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 16.0,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w300
                            )
                          )
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 16.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: Text(
                            'I have account! Login',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 15,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal
                            ),
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 16.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFE4E4E4)
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFF0084F4)
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFE4E4E4)
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
          
        ],
      ),
    );
  }
}

class PasswordRegister extends StatefulWidget {
  String username;
  String email;

  PasswordRegister({this.username, this.email, Key key}) : super(key: key);

  @override
  _PasswordRegisterState createState() => _PasswordRegisterState();
}

class _PasswordRegisterState extends State<PasswordRegister> {
  TextEditingController _passwordController;
  TextEditingController _confirmController;

  @override
  void initState() {
    // TODO: implement initState
    _passwordController = TextEditingController();
    _confirmController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // background section
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/luca.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                // color: Color(0xFFE4E4E4).withOpacity(0.5),
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                    Colors.black.withOpacity(0.5),
                  ]
                )
              ),
            ),

          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                // top bar section
                Container(
                  padding: EdgeInsets.only(top: 48.14, left: 25.0),
                  alignment: Alignment.topCenter,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(width: 120,),
                      Text(
                        'Register',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),

                // middle bar section
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Create an account',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 28.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      SizedBox(height: 15.0,),
                      Text(
                        'Signing up or login to see\nour top picks for you',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Roboto',
                          fontSize: 17.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w300
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),

                // bottom bar section
                Container(
                  margin: EdgeInsets.all(25.0),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please fill the password field box';
                          } else {
                            return null;
                          }
                        },
                        controller: _passwordController,
                        decoration: InputDecoration(
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.black
                            ),
                          ),
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue)
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red)
                          ),
                          labelText: 'Enter Your Password',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(Icons.lock, color: Colors.black,),
                        ),
                      ),

                      SizedBox(height: 10.0,),

                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please fill the confirm password field box';
                          } else {
                            return null;
                          }
                        },
                        controller: _passwordController,
                        decoration: InputDecoration(
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.black
                            ),
                          ),
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue)
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.red)
                          ),
                          labelText: 'Confirm Your Password',
                          labelStyle: TextStyle(color: Colors.black),
                          prefixIcon: Icon(Icons.lock, color: Colors.black,),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 20.0),
                        height: 60,
                        decoration: BoxDecoration(
                          color: Color(0xFF0084F4),
                          borderRadius: BorderRadius.circular(5.0)
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: FlatButton(
                          onPressed: () {}, 
                          child: Text(
                            'Next', 
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 16.0,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w300
                            )
                          )
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 16.0),
                        child: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: Text(
                            'I have account! Login',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 15,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal
                            ),
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 16.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFE4E4E4)
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFE4E4E4)
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Container(
                              height: 8.0,
                              width: 8.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFF0084F4)
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
          
        ],
      ),
    );
  }
}