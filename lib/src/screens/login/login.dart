import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController _usernameController;
  TextEditingController _passwordController;
  bool _value1 = false;
  bool _value2 = false;

  //we omitted the brackets '{}' and are using fat arrow '=>' instead, this is dart syntax
  void _value1Changed(bool value) => setState(() => _value1 = value);
  void _value2Changed(bool value) => setState(() => _value2 = value);

  @override
  void initState() {
    // TODO: implement initState
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            // background section
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/clarisse.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(

                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5),
                        Colors.black.withOpacity(0.5),
                      ]
                  )
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[


                  // middle bar section
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Let\'s Get Started',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 28.0,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                        SizedBox(height: 15.0,),
                        Text(
                          'Login to see\nour top favorite places for you.',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 17.0,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w300
                          ),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),

                  // bottom bar section
                  Container(
                    margin: EdgeInsets.all(25.0),
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please fill the Email field box';
                            } else {
                              return null;
                            }
                          },
                          controller: _usernameController,
                          decoration: InputDecoration(
                            filled: true,
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.black
                              ),
                            ),
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue)
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red)
                            ),
                            labelText: 'Enter Username',
                            labelStyle: TextStyle(color: Colors.black),
                            prefixIcon: Icon(Icons.person, color: Colors.black,),
                          ),
                        ),

                        SizedBox(height: 8.0,),

                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please fill the password field box';
                            } else {
                              return null;
                            }
                          },
                          controller: _passwordController,
                          decoration: InputDecoration(
                            filled: true,
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.black
                              ),
                            ),
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue)
                            ),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red)
                            ),
                            labelText: 'Enter Password',
                            labelStyle: TextStyle(color: Colors.black),
                            prefixIcon: Icon(Icons.lock, color: Colors.black,),
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 20.0),
                          height: 60,
                          decoration: BoxDecoration(
                              color: Color(0xFF0084F4),
                              borderRadius: BorderRadius.circular(5.0)
                          ),
                          width: MediaQuery.of(context).size.width,
                          child: FlatButton(
                              onPressed: () {
                                Navigator.pushNamed(context, '/home');
                              },
                              child: Text(
                                  'Get Started',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'Roboto',
                                      fontSize: 16.0,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w300
                                  )
                              )
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 16.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/register');
                            },
                            child: Text(
                              'Create new account!',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Roboto',
                                  fontSize: 15,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
      // body: Column(
      //   children: <Widget>[
      //     Expanded(
      //         flex: 1,
      //         child: Stack(
      //           alignment: Alignment.center,
      //           children: <Widget>[
      //             Container(
      //               decoration: BoxDecoration(
      //                 image: DecorationImage(
      //                   image: AssetImage('assets/images/luca.jpg'),
      //                   fit: BoxFit.fitHeight,
      //                 ),
      //               ),
      //             ),
      //
      //             Container(
      //               width: MediaQuery.of(context).size.width,
      //               decoration: BoxDecoration(
      //                   gradient: LinearGradient(
      //                       begin: Alignment.bottomCenter,
      //                       end: Alignment.topCenter,
      //                       colors: [
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                         Colors.black.withOpacity(0.5),
      //                       ]
      //                   )
      //               ),
      //             ),
      //
      //             Column(
      //               mainAxisSize: MainAxisSize.min,
      //               mainAxisAlignment: MainAxisAlignment.center,
      //               children: <Widget>[
      //                 Container(
      //                   child: Text(
      //                     'Let\'s Get Started',
      //                     style: TextStyle(
      //                         color: Colors.white,
      //                         fontWeight: FontWeight.w500,
      //                         fontStyle: FontStyle.normal,
      //                         fontSize: 28,
      //                         fontFamily: 'Roboto'
      //                     ),
      //                   ),
      //                 ),
      //                 SizedBox(height: 16,),
      //                 Container(
      //                   child: Text(
      //                     'Let\'s Get StartedLogin to see\nour top favorite places for you.',
      //                     style: TextStyle(
      //                         color: Colors.white,
      //                         fontSize: 17,
      //                         fontWeight: FontWeight.normal,
      //                         fontStyle: FontStyle.normal,
      //                         fontFamily: 'Roboto'
      //                     ),
      //                     textAlign: TextAlign.center,
      //                   ),
      //                 ),
      //               ],
      //             ),
      //
      //           ],
      //         )
      //     ),
      //
      //     Expanded(
      //       flex: 1,
      //       child: Container(
      //         padding: EdgeInsets.only(left: 25, right: 25, top: 20, bottom: 20),
      //         width: MediaQuery.of(context).size.width,
      //         decoration: BoxDecoration(
      //             color: Colors.white,
      //             borderRadius: BorderRadius.only(
      //               topLeft: Radius.circular(16.0),
      //               topRight: Radius.circular(16.0),
      //             )
      //         ),
      //         child: Column(
      //           mainAxisAlignment: MainAxisAlignment.start,
      //           children: <Widget>[
      //             Text(
      //               'Let\'s Go Travel',
      //               textAlign: TextAlign.left,
      //               style: TextStyle(
      //                   color: Colors.black,
      //                   fontFamily: 'Roboto',
      //                   fontSize: 28
      //               ),
      //             ),
      //
      //             Container(
      //               padding: EdgeInsets.only(top: 15.0),
      //               child: TextFormField(
      //                 validator: (value) {
      //                   if (value.isEmpty) {
      //                     return 'Please fill the username field box';
      //                   } else {
      //                     return null;
      //                   }
      //                 },
      //                 controller: _usernameController,
      //                 decoration: InputDecoration(
      //                   enabledBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(
      //                           color: Colors.black
      //                       )
      //                   ),
      //                   focusedBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(color: Colors.blue)
      //                   ),
      //                   border: OutlineInputBorder(
      //                       borderSide: BorderSide(color: Colors.red)
      //                   ),
      //                   labelText: 'Username',
      //                   labelStyle: TextStyle(color: Colors.black),
      //                   prefixIcon: Icon(Icons.person, color: Colors.black,),
      //                 ),
      //               ),
      //             ),
      //
      //             Container(
      //               padding: EdgeInsets.only(top: 15.0),
      //               child: TextFormField(
      //                 validator: (value) {
      //                   if (value.isEmpty) {
      //                     return 'Please fill the password field box';
      //                   } else {
      //                     return null;
      //                   }
      //                 },
      //                 controller: _passwordController,
      //                 decoration: InputDecoration(
      //                   enabledBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(
      //                           color: Colors.black
      //                       )
      //                   ),
      //                   focusedBorder: OutlineInputBorder(
      //                       borderSide: BorderSide(color: Colors.blue)
      //                   ),
      //                   border: OutlineInputBorder(
      //                       borderSide: BorderSide(color: Colors.red)
      //                   ),
      //                   labelText: 'Password',
      //                   labelStyle: TextStyle(color: Colors.black),
      //                   prefixIcon: Icon(Icons.lock, color: Colors.black,),
      //                 ),
      //               ),
      //             ),
      //
      //             // Container(
      //             //   padding: EdgeInsets.only(top: 10.0),
      //             //   height: 40.0,
      //             //   // alignment: Alignment.topLeft,
      //             //   child: Row(
      //             //     mainAxisSize: MainAxisSize.min,
      //             //     mainAxisAlignment: MainAxisAlignment.start,
      //             //     children: <Widget>[
      //             //       Checkbox(
      //             //         value: _value1,
      //             //         onChanged: _value1Changed,
      //             //       ),
      //             //       CheckboxListTile(
      //             //         value: _value2,
      //             //         onChanged: _value2Changed,
      //             //         title: Text('Remember me',
      //             //           style: TextStyle(
      //             //             fontSize: 11,
      //             //             fontFamily: 'Roboto',
      //             //             fontStyle: FontStyle.normal,
      //             //             fontWeight: FontWeight.normal,
      //             //             color: Color(0x00151522)
      //             //           ),
      //             //         ),
      //             //         controlAffinity: ListTileControlAffinity.leading,
      //             //         secondary: Icon(Icons.archive),
      //             //         activeColor: Colors.red,
      //             //       )
      //             //     ],
      //             //   ),
      //             // ),
      //
      //             Container(
      //               decoration: BoxDecoration(
      //                   color: Color(0xFF0084F4),
      //                   borderRadius: BorderRadius.circular(5.0)
      //               ),
      //               width: MediaQuery.of(context).size.width,
      //               margin: EdgeInsets.only(top: 15.0),
      //               child: FlatButton(
      //                 onPressed: () {
      //
      //                 },
      //                 child: Text(
      //                   'Get Started',
      //                   style: TextStyle(
      //                       color: Colors.white,
      //                       fontFamily: 'Roboto',
      //                       fontSize: 16.0,
      //                       fontWeight: FontWeight.w300,
      //                       fontStyle: FontStyle.normal
      //                   ),
      //                 ),
      //               ),
      //             ),
      //
      //             Container(
      //               padding: EdgeInsets.only(top: 20.0),
      //               child: FlatButton(
      //                 onPressed: () {
      //                   Navigator.pushNamed(
      //                       context,
      //                       '/register'
      //                   );
      //                 },
      //                 child: Text(
      //                   'Create new account!',
      //                   style: TextStyle(
      //                       color: Color(0xFF0084F4),
      //                       fontFamily: 'Roboto',
      //                       fontSize: 15.0,
      //                       fontStyle: FontStyle.normal,
      //                       fontWeight: FontWeight.normal
      //                   ),
      //                 ),
      //               ),
      //             )
      //           ],
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}