import 'package:flutter/material.dart';
import 'package:travel_apps/src/screens/home/account/account.dart';
import 'package:travel_apps/src/screens/home/dashboard/dashboard.dart';
import 'package:travel_apps/src/screens/home/favorite/favorite.dart';
import 'package:travel_apps/src/screens/home/search/search.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  static List<Widget> _listScreen = <Widget>[
    DashboardScreen(),
    SearchScreen(),
    FavoriteScreen(),
    AccountScreen()
  ];

  int _currentIndex = 0;

  void _onItemTap(index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Future<bool> _exitApp() {
    return showDialog(
        context: context,
      builder: (context) => AlertDialog(
        title: Text('Exit App'),
        content: Text('Do you want to exit this App ?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('No'),
          ),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text('Yes'),
          ),
        ],
      )
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(child: _listScreen.elementAt(_currentIndex), onWillPop: _exitApp),
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home, color: Colors.black,),
              title: Text('Dashboard', style: TextStyle(color: Colors.black),)
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.search, color: Colors.black),
                title: Text('Search', style: TextStyle(color: Colors.black),)
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite, color: Colors.black),
                title: Text('Favorite', style: TextStyle(color: Colors.black),)
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person, color: Colors.black),
                title: Text('Account', style: TextStyle(color: Colors.black),)
            ),
          ],
        onTap: _onItemTap,
        selectedItemColor: Colors.blue,
        currentIndex: _currentIndex,
      ),
    );
  }
}