import 'package:flutter/material.dart';

class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        actions: <Widget>[
          FlatButton(
            onPressed: () {},
            child: Icon(Icons.notifications, color: Colors.blue,),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16.0,),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Text("Mau liburan\nKemana nih ?", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
          ),
          SizedBox(height: 16.0,),

          //  Menu section
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: BorderSide(color: Colors.blue)
                    ),
                    color: Colors.blue,
                    child: Container(height: 80.0, padding: EdgeInsets.all(8.0), child: Icon(Icons.hotel, color: Colors.white, size: 32,)),
                  ),
                  SizedBox(height: 8.0,),
                  Text('Hotel', style: TextStyle(color: Colors.black), textAlign: TextAlign.center,)
                ],
              ),

              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: BorderSide(color: Colors.blue)
                    ),
                    color: Colors.blue,
                    child: Container(height: 80.0, padding: EdgeInsets.all(8.0), child: Icon(Icons.airplanemode_active, color: Colors.white, size: 32,)),
                  ),
                  SizedBox(height: 8.0,),
                  Text('Plane', style: TextStyle(color: Colors.black), textAlign: TextAlign.center,)
                ],
              ),

              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: BorderSide(color: Colors.blue)
                    ),
                    color: Colors.blue,
                    child: Container(height: 80.0, padding: EdgeInsets.all(8.0), child: Icon(Icons.train, color: Colors.white, size: 32,)),
                  ),
                  SizedBox(height: 8.0,),
                  Text('Train', style: TextStyle(color: Colors.black), textAlign: TextAlign.center,)
                ],
              ),

              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        side: BorderSide(color: Colors.blue)
                    ),
                    color: Colors.blue,
                    child: Container(height: 80.0, padding: EdgeInsets.all(8.0), child: Icon(Icons.directions_bus, color: Colors.white, size: 32,)),
                  ),
                  SizedBox(height: 8.0,),
                  Text('Bus', style: TextStyle(color: Colors.black), textAlign: TextAlign.center,)
                ],
              ),
              
            ],
          ),
          SizedBox(height: 16.0,),

          // Trending Places Section
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text('Trending Places', style: TextStyle(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),),
                    Spacer(),
                    FlatButton(onPressed: () {}, child: Text('View All', style: TextStyle(color: Colors.blue),),)
                  ],
                ),
              ),

              Container(
                width: MediaQuery.of(context).size.width,
                height: 180.0,
                color: Colors.white,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(8.0),
                                bottomLeft: Radius.circular(8.0),
                              ),
                              color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),
                            child: Image.asset("assets/images/land.jpg", height: 120.0, width: 120.0, fit: BoxFit.cover),
                          ),
                          Container(
                            height: 52.0,
                            margin: EdgeInsets.only(bottom: 8.0),
                            width: 120.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    blurRadius: 8.0,
                                  )
                                ]
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Pantai Kuta", style: TextStyle(color: Colors.black, fontSize: 16.0), textAlign: TextAlign.left, maxLines: 1,),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text("Bali, Indonesia", style: TextStyle(color: Colors.grey[500], fontSize: 12.0), textAlign: TextAlign.left, maxLines: 1,),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0,),

          // Best Packages section
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text('Best Packages', style: TextStyle(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),),
                    Spacer(),
                    FlatButton(onPressed: () {}, child: Text('View All', style: TextStyle(color: Colors.blue),),)
                  ],
                ),
              ),

              Container(
                width: MediaQuery.of(context).size.width,
                height: 100.0,
                color: Colors.white,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    FlatButton(
                      onPressed: () {},
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset("assets/images/land.jpg", height: 100, width: 160.0, fit: BoxFit.fill, alignment: Alignment.center,),
                          ),
                          Positioned(
                            child: Text('Pantai Kuta', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            bottom: 4.0,
                            left: 8.0,
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset("assets/images/land.jpg", height: 100, width: 160.0, fit: BoxFit.fill, alignment: Alignment.center,),
                          ),
                          Positioned(
                            child: Text('Pantai Kuta', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            bottom: 4.0,
                            left: 8.0,
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset("assets/images/land.jpg", height: 100, width: 160.0, fit: BoxFit.fill, alignment: Alignment.center,),
                          ),
                          Positioned(
                            child: Text('Pantai Kuta', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            bottom: 4.0,
                            left: 8.0,
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset("assets/images/land.jpg", height: 100, width: 160.0, fit: BoxFit.fill, alignment: Alignment.center,),
                          ),
                          Positioned(
                            child: Text('Pantai Kuta', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            bottom: 4.0,
                            left: 8.0,
                          ),
                        ],
                      ),
                    ),

                    FlatButton(
                      onPressed: () {},
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset("assets/images/land.jpg", height: 100, width: 160.0, fit: BoxFit.fill, alignment: Alignment.center,),
                          ),
                          Positioned(
                            child: Text('Pantai Kuta', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            bottom: 4.0,
                            left: 8.0,
                          ),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0,),
        ],
      )
    );
  }
}