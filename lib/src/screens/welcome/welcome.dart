import 'package:flutter/material.dart';
import '../login/login.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Welcome Page'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Center(
              child: RaisedButton(onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName('/login') 
                );},
                child: Text('to Login'),
              ),
            ),

            RaisedButton(
              onPressed: () {
                customDialog(context);
              },
              child: Text('to dialog'),
            )
          ],
        ),
      ),
    );
  }

  customDialog(context) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: Colors.white,
          child: Container(
            width: MediaQuery.of(context).size.width - 200,
            child: Wrap(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    child: Image.asset(
                      "assets/images/luca.jpg", 
                      height: 300,
                      fit: BoxFit.cover, 
                      width: MediaQuery.of(context).size.width * 0.5,
                    ),
                    margin: EdgeInsets.only(bottom: 16, top: 16),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "kamu bekerja keras hari ini,\nhati-hati di jalan ya!",
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 16,),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue,
                        child: FlatButton(
                          onPressed: (){}, 
                          child: Text(
                            "Okay!",
                            style: TextStyle(
                              color: Colors.white
                            ),
                          )
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      } 
    );
  }
}