import 'package:flutter/material.dart';
import 'package:travel_apps/src/screens/home/account/account.dart';
import 'package:travel_apps/src/screens/home/dashboard/dashboard.dart';
import 'package:travel_apps/src/screens/home/favorite/favorite.dart';
import 'package:travel_apps/src/screens/home/search/search.dart';
import 'src/screens/home/home.dart';
import 'src/screens/login/login.dart';
import 'src/screens/register/register.dart';
import 'src/screens/splash/splash.dart';
import 'src/screens/welcome/welcome.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

Color firstColor = Color(0xFF0084F4);
Color secondColor = Color(0xFF03A9F4);

ThemeData appTheme = ThemeData(
    primaryColor: firstColor,
    fontFamily: 'Oxygen'
);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: appTheme,
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => SplashScreen(),
        '/login': (BuildContext context) => LoginScreen(),
        '/register': (BuildContext context) => RegisterScreen(),
        '/emailRegister': (BuildContext context) => EmailRegister(),
        '/passwordRegister': (BuildContext context) => PasswordRegister(),
        '/home': (BuildContext context) => HomeScreen(),
        '/welcome': (BuildContext context) => WelcomeScreen(),
        '/dashboard': (BuildContext context) => DashboardScreen(),
        '/search': (BuildContext context) => SearchScreen(),
        '/favorite': (BuildContext context) => FavoriteScreen(),
        '/account': (BuildContext context) => AccountScreen(),
      },
    );
  }
}